const Client = require('../index');

let assert = require('assert');
describe('Client', () => {
  let stride;
  beforeEach(() => {
    stride = new Client({});
  });
  describe('utils', () => {
    let doc;
    beforeEach(() => {
      doc = require('./message.test.json');
    });
    describe('getMentionIdsFromADF', () => {
      it('should return a list of user ids', function() {
        let actual = stride.utils.getMentionIdsFromADF(doc);
        let expected = [
          '9491f5dfd03a44208f5f1aeb84ffcfae',
          '442233:a7cd7a76-9133-4e03-a1dc-5b238d64be5a',
          '122223:d994c418-b222-4b9c-846f-46ab10aaba52',
          '122222:091e590a-f867-46bf-b007-e9b6d801e3ea',
          '333333:cae6d887-017a-4805-acf8-5107de84d8db',
          'b02308d0676d45168fbf61cef84bee6b',
        ];
        assert.deepEqual(actual, expected);
      });
      it('should return an empty list when document has no mentions', function() {
        let actual = stride.utils.getMentionIdsFromADF({});
        let expected = [];
        assert.deepEqual(actual, expected);
      });
    });
    describe('isUserIdMentionedInMessage', () => {
      it('should return true if a user id is found', function() {
        let actual = stride.utils.isUserIdMentionedInMessage(
          doc,
          '333333:cae6d887-017a-4805-acf8-5107de84d8db'
        );
        let expected = true;
        assert.deepEqual(actual, expected);
      });
      it('should return false if a user id is not found', function() {
        let actual = stride.utils.isUserIdMentionedInMessage(doc, "I'm not here");
        let expected = false;
        assert.deepEqual(actual, expected);
      });
    });
  });
});
