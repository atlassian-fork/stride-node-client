/**
 *
 *
 *  @type {Object}
 *  @name Configuration
 */
class Configuration {
  constructor(client) {
    this.client = client;
  }

  /**
   * @deprecated use updateConfigurationState method from Modules class instead
   * @name Configuration.Configuration State Update
   * @description Update Configuration State
   * @param {string} configKey Key used to describe the type of configuration you are getting or setting, e.g. sidebar-showcase, configuration, glance-showcase.
   * This is done in the descriptor file.
   * @param {object} opts opts.body = { "context": { cloudId, conversationId }, "configured": true || false }
   * @return {empty} none
   * @see {@link https://developer.atlassian.com/cloud/stride/rest/#api-app-module-chat-conversation-chat-configuration-key-state-post | Config State Update }
   *
   * ``` body = { "context": { cloudId, conversationId }, "configured": state } ```
   *
   */

  async updateState(configurationKey, opts) {
    const endpoint = `/app/module/chat/conversation/chat:configuration/${configurationKey}/state`;
    return this.client.post(endpoint, opts);
  }
}

module.exports = function(client) {
  return new Configuration(client);
};
