/**
 *
 *
 *  @type {Object}
 *  @name Module
 */
class Modules {
  constructor(client) {
    this.client = client;
  }

  /**
   * @name Modules. List chat:bot:messages modules
   * @description List chat:bot:messages modules.
   * @param {string} cloudId Commonly referred to as siteId, this is the site the user belongs to in Stride.
   * @param {string} conversationId The ID of the conversation to create the module for.
   * @param {string} start Offset for the query.
   * @param {string} limit A maximum number of modules to return per call. Valid length range: 1 - 75. Default value is 10.
   * @param {object} opts none
   * @returns {empty} No content
   * @see {@link https://developer.atlassian.com/cloud/stride/rest/#api-site-cloudId-conversation-conversationId-app-module-chat-bot-messages-get | List chat:bot:messages modules }
   */
  list_chat_bot_messages_modules(cloudId, conversationId, start, limit, opts) {
    const endpoint = `/site/${cloudId}/conversation/${conversationId}/app/module/chat:bot:messages?start=${start}&${limit}`;
    return this.client.get(endpoint, opts);
  }

  /**
   * @name Modules. Create or update a chat:bot:messages module
   * @description Create or update a chat:bot:messages module.
   * @param {string} cloudId Commonly referred to as siteId, this is the site the user belongs to in Stride.
   * @param {string} conversationId The ID of the conversation to create the module for.
   * @param {string} moduleKey A key which will be used to identify the created module.
   * @param {object} opts none
   * @returns {empty} No content
   * @see {@link https://developer.atlassian.com/cloud/stride/rest/#api-site-cloudId-conversation-conversationId-app-module-chat-bot-messages-key-put | Create or update a chat:bot:messages module }
   */
  create_or_update_chat_bot_messages_module(cloudId, conversationId, moduleKey, opts) {
    const endpoint = `/site/${cloudId}/conversation/${conversationId}/app/module/chat:bot:messages/${moduleKey}`;
    return this.client.put(endpoint, opts);
  }

  /**
   * @name Modules. Delete a chat:bot:messages module
   * @description Delete a chat:bot:messages module.
   * @param {string} cloudId Commonly referred to as siteId, this is the site the user belongs to in Stride.
   * @param {string} conversationId The ID of the conversation to create the module for.
   * @param {string} moduleKey A key which will be used to identify the created module.
   * @param {object} opts none
   * @returns {empty} No content
   * @see {@link https://developer.atlassian.com/cloud/stride/rest/#api-site-cloudId-conversation-conversationId-app-module-chat-bot-messages-key-delete | Delete a chat:bot:messages module }
   */
  delete_chat_bot_messages_module(cloudId, conversationId, moduleKey) {
    const endpoint = `/site/${cloudId}/conversation/${conversationId}/app/module/chat:bot:messages/${moduleKey}`;
    return this.client.del(endpoint);
  }

  /**
   * @name Modules. List chat:webhook modules
   * @description List chat:webhook modules.
   * @param {string} cloudId Commonly referred to as siteId, this is the site the user belongs to in Stride.
   * @param {string} conversationId The ID of the conversation to create the module for.
   * @param {string} start Offset for the query.
   * @param {string} limit A maximum number of modules to return per call. Valid length range: 1 - 75. Default value is 10.
   * @param {object} opts none
   * @returns {empty} No content
   * @see {@link https://developer.atlassian.com/cloud/stride/rest/#api-site-cloudId-conversation-conversationId-app-module-chat-webhook-get | List chat:webhook modules }
   */
  list_chat_webhook_modules(cloudId, conversationId, start, limit, opts) {
    const endpoint = `/site/${cloudId}/conversation/${conversationId}/app/module/chat:webhook?start=${start}&${limit}`;
    return this.client.get(endpoint, opts);
  }

  /**
   * @name Modules. Create or update a chat:webhook module
   * @description Create or update a chat:webhook module
   * @param {string} cloudId Commonly referred to as siteId, this is the site the user belongs to in Stride.
   * @param {string} conversationId The ID of the conversation to create the module for.
   * @param {string} moduleKey A key which will be used to identify the created module.
   * @param {object} opts none
   * @returns {empty} No content
   * @see {@link https://developer.atlassian.com/cloud/stride/rest/#api-site-cloudId-conversation-conversationId-app-module-chat-webhook-key-put | Create or update a chat:webhook module }
   */
  create_or_update_chat_webhook_module(cloudId, conversationId, moduleKey, opts) {
    const endpoint = `/site/${cloudId}/conversation/${conversationId}/app/module/chat:webhook/${moduleKey}`;
    return this.client.put(endpoint, opts);
  }

  /**
   * @name Modules. Delete a chat:webhook module
   * @description Delete a chat:webhook module.
   * @param {string} cloudId Commonly referred to as siteId, this is the site the user belongs to in Stride.
   * @param {string} conversationId The ID of the conversation to create the module for.
   * @param {string} moduleKey A key which will be used to identify the created module.
   * @param {object} opts none
   * @returns {empty} No content
   * @see {@link https://developer.atlassian.com/cloud/stride/rest/#api-site-cloudId-conversation-conversationId-app-module-chat-webhook-key-delete | Delete a chat:webhook module }
   */
  delete_chat_webhook_module(cloudId, conversationId, moduleKey) {
    const endpoint = `/site/${cloudId}/conversation/${conversationId}/app/module/chat:webhook/${moduleKey}`;
    return this.client.del(endpoint);
  }

  /**
   * @name Modules. Create or update a chat:bot module
   * @description Create or update a chat:bot module
   * @param {string} cloudId Commonly referred to as siteId, this is the site the user belongs to in Stride.
   * @param {string} conversationId The ID of the conversation to create the module for.
   * @param {string} moduleKey A key which will be used to identify the created module.
   * @param {object} opts none
   * @returns {empty} No content
   * @see {@link https://developer.atlassian.com/cloud/stride/rest/#api-site-cloudId-conversation-conversationId-app-module-chat-bot-key-put | Create or update a chat:bot module }
   */
  create_or_update_chat_bot_module(cloudId, conversationId, moduleKey, opts) {
    const endpoint = `/site/${cloudId}/conversation/${conversationId}/app/module/chat:bot/${moduleKey}`;
    return this.client.put(endpoint, opts);
  }

  /**
   * @name Webhooks. Delete a chat:bot module
   * @description Delete a chat:bot module.
   * @param {string} cloudId Commonly referred to as siteId, this is the site the user belongs to in Stride.
   * @param {string} conversationId The ID of the conversation to create the module for.
   * @param {string} moduleKey A key which will be used to identify the created module.
   * @param {object} opts none
   * @returns {empty} No content
   * @see {@link https://developer.atlassian.com/cloud/stride/rest/#api-site-cloudId-conversation-conversationId-app-module-chat-bot-key-delete | Delete a chat:bot module }
   */
  delete_chat_bot_module(cloudId, conversationId, moduleKey) {
    const endpoint = `/site/${cloudId}/conversation/${conversationId}/app/module/chat:bot/${moduleKey}`;
    return this.client.del(endpoint);
  }

  /**
   * @name Modules. Update chat:configuration state
   * @description Update app's chat:configuration module state.
   * @param {string} cloudId Commonly referred to as siteId, this is the site the user belongs to in Stride.
   * @param {string} conversationId The ID of the conversation to create the module for.
   * @param {string} key A key which will be used to identify the chat:configuration module.
   * @param {object} opts none
   * @returns {empty} No content
   * @see {@link https://developer.atlassian.com/cloud/stride/rest/#api-site-cloudId-conversation-conversationId-app-module-chat-configuration-key-state-put | Update app configuration state }
   */
  updateConfigurationState(cloudId, conversationId, key, opts) {
    const endpoint = `/site/${cloudId}/conversation/${conversationId}/app/module/chat:configuration/${key}/state`;
    return this.client.put(endpoint, opts);
  }

  /**
   * @name  Modules. Update chat:glance state
   * @description  Updates chat:glance module state.
   * @param {string} cloudId Commonly referred to as siteId, this is the site the user belongs to in Stride.
   * @param {string} conversationId The id of the conversation where the media will be sent.
   * @param {string} glanceKey A key which will be used to identify the chat:glance module.
   * @param {string} opts body = {  label: string, metadata: {} }
   * @return {empty} none
   * @link https://developer.atlassian.com/cloud/stride/rest/#api-site-cloudId-conversation-conversationId-app-module-chat-glance-key-state-put
   */
  updateGlanceState(cloudId, conversationId, glanceKey, opts) {
    const endpoint = `/site/${cloudId}/conversation/${conversationId}/app/module/chat:glance/${glanceKey}/state`;
    return this.client.put(endpoint, opts);
  }
}

module.exports = function(client) {
  return new Modules(client);
};
