const rp = require('request-promise');

/**
 * @description Called by stride client to generate oauth token for outgoing requests.
 * @name Authentication
 * @param {string} clientId string client id of the app.
 * @param {string} secret string client secret of the app.
 * @param {string} baseUrl API environment URL to generate Oauth Token
 * @return {string} returns an oauth token in string format.
 * @see {@link https://developer.atlassian.com/cloud/stride/security/authentication/ | Authentication }
 ```
 async function oauth(clientId, secret, baseUrl) {
	const options = {
		uri: baseUrl + '/oauth/token',
		method: 'POST',
		json: {
			grant_type: 'client_credentials',
			client_id: clientId,
			client_secret: secret,
			audience: baseUrl.replace('https://', ''),
		},
	};
```
*/
let token = null;
let accessTokenPromise = null;
async function getAccessTokenInt(clientId, secret, baseUrl) {
  if (token && Date.now() <= token.refresh_time) {
    // Reuse the cached token if any
    return token.access_token;
  }

  if (accessTokenPromise) {
    // a request for a token is in-flight, don't duplicate it
    return await accessTokenPromise;
  }

  // Generate a new token
  const request_options = {
    uri: baseUrl + '/oauth/token',
    method: 'POST',
    json: {
      grant_type: 'client_credentials',
      client_id: clientId,
      client_secret: secret,
    },
  };

  accessTokenPromise = rp(request_options)
    .then(newToken => {
      // remember to refresh the token a minute before it expires (tokens last for an hour)
      newToken.refresh_time = Date.now() + (newToken.expires_in - 60) * 1000;
      token = newToken;

      // The promise is now getting resolved so set it to null again to allow future
      // requests to go through
      accessTokenPromise = null;

      return token.access_token;
    })
    .catch(reason => {
      token = null;
      accessTokenPromise = null;

      let err;
      if (reason.response && reason.response.headers) {
        err = new Error(
          `error retrieving access token - ${reason.error.error}:${
            reason.error.error_description
          } (Auth0 trace ID ${reason.response.headers['x-auth0-requestid']})`
        );
      } else {
        err = new Error(`error retrieving access token - ${reason.name}:${reason.message}`);
      }

      err.reason = reason;
      throw err;
    });

  return await accessTokenPromise;
}

async function getAccessToken(clientId, secret, baseUrl) {
  let tries = 0;
  let lastError = null;
  while (tries < 3) {
    try {
      const accessToken = await getAccessTokenInt(clientId, secret, baseUrl);

      if (!accessToken) {
        tries++;
      } else {
        return accessToken;
      }
    } catch (e) {
      lastError = e;
      tries++;
    }
  }
  // If we haven't gotten an access token after 3 tries...
  const err = new Error(
    `unable to generate token. check your NODE_ENV is production, CLIENT_ID and CLIENT_SECRET values are correct. ${lastError}`
  );
  err.reason = lastError.reason;
  err.lastError = lastError;
  throw err;
}

module.exports = getAccessToken;
