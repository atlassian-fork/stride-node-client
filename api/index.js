const generateToken = require('./endpoints/authentication');

/**
 * @ignore
 * @name Request
 * @description Responsible for binding and building a stride request.
 * @param auth clientId and secret passed from client
 * @param method Request method  GET | POST | PUT | DEL
 * @param endpoint Stride API URL to call
 * @param opts
 * <ol>
 *     <li>opts.body </li>
 *     <li>opts.headers To override defaults application/json, or to add additional headers. </li>
 *     <li> opts.qs to include any query string parameters in the request. </li>
 *     </ol>
 * @return {object}

 * */
module.exports = async function apiRequest(auth, method, endpoint, opts) {
  const { clientId, secret, baseUrl } = auth;
  try {
    const token = await generateToken(clientId, secret, baseUrl);
    const req = {
      uri: `${baseUrl}${endpoint}`,
      method,
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
        accept: 'application/json',
      },
      json: true,
    };

    //Handle DELETE method case
    if (req.method === 'DELETE') {
      delete req['body'];
    }

    // Handle POST use case
    if (req.method === 'POST' || req.method === 'PUT' || req.method === 'PATCH') {
      // A Post and PUT changes state.
      if (opts.headers) req.headers = Object.assign({}, req.headers, opts.headers);
      if (opts.body) req.body = opts.body;

      if (
        req.headers['Content-Type'] === 'text/plain' ||
        req.headers['Content-Type'] === 'text/markdown' ||
        req.headers['Content-Type'] === 'application/octet-stream'
      ) {
        delete req['json'];
      }
    }

    // Handle GET use case
    if (req.method === 'GET') {
      if (opts.headers) req.headers = Object.assign({}, req.headers, opts.headers);
      if (opts.qs) req.qs = Object.assign({}, req.qs, opts.qs);
      delete req['body'];
      delete req['Content-Type'];
    }

    if (req.method === 'OPTIONS') {
      if (opts.headers) req.headers = Object.assign({}, req.headers, opts.headers);
      req.json = false;
      delete req.headers['Content-Type'];
      delete req['body'];
    }

    return sanitizeReq(req);
  } catch (err) {
    throw new Error(`Error while generating JWT token for your request ${endpoint}:  ${err}`);
  }
};

function sanitizeReq(req) {
  // support lower case content-type
  if (req.headers['content-type'] && req.headers['Content-Type']) {
    //delete the key that was part if the default request object.
    delete req['headers']['Content-Type'];
  }

  return req;
}
